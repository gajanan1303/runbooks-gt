{
  resolveRecordingRuleFor(
    aggregationFunction='sum',
    aggregationLabels=[],
    rangeVectorFunction='rate',
    metricName=null,
    rangeInterval='5m',
    selector={},
    offset=null
  ):: null,

  recordingRuleForMetricAtBurnRate(metricName, rangeInterval):: false,
}
